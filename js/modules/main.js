/*jshint esversion: 6 */
import {Window} from './window';
import {Renderer} from './renderer';
import {VisualObject} from './visual-object';
import {Process} from './process';
import {ProcessContainer} from './process-container';
import {InfoField} from './info-field';
import {randBetween, roundToDecPlaces} from './utility';
import {Controls} from './controls';
import {RoundRobin} from './roundrobin';
import {GraphicalInfoField} from './graphical-info-field';

var quantum = 130;
var avgWait = 0;
var processCount = 4;
var randFrom = 1;
var randTo = 300;

var rend = new Renderer("#canvas");
var positions = rend.offsetByPercFromCanvSides(0.1, 0.4);
var infoPos = rend.offsetByPercFromCanvSides(0.05, 0.1);
var visualInfoPos = rend.offsetByPercFromCanvSides(0.85, 0.1);

var visualInfo = new GraphicalInfoField(rend, visualInfoPos[0], ["assets/spacebar.png",
                                                                 "assets/key_m.png",
                                                                 "assets/key_r.png"],

                                                                                            ["Stop / Play",
                                                                                             "Change settings",
                                                                                             "Restart"]);

var controls = new Controls();

var processCont = null;
var info = null;
var algo = null;
var processData = null;

function initVizualization()
{
  processCont = new ProcessContainer(rend, positions[0], positions[1][0], processCount);
  info = new InfoField(rend, infoPos[0], "Round robin algorithm", ["time quantum: ", "average wait time: "], [quantum, avgWait]);
  algo = new RoundRobin(quantum, processCont, info, 1);

  for(var i = 0; i < processCount; i++)
  {
    processCont.addProcessInitPos(processData[i][0], processData[i][1]);
  }
}

function generateProcessData()
{
  processData = [];

  for(var i = 0; i < processCount; i++)
  {
    var burst = randBetween(randFrom, randTo);
    var id = "P" + i.toString();
    processData.push([id, burst]);
  }
}

function updateParameters(parameters)
{
  quantum = parameters[0];
  processCount = parameters[1];
  randFrom = parameters[2];
  randTo = parameters[3];

}

generateProcessData();
initVizualization();


function mainLoop()
{
  rend.clear();
  info.render();
  visualInfo.render();
  processCont.render();
  processCont.animate(6);

  if(controls.paused == false)
    algo.update();

  if(controls.checkIfRestart())
    initVizualization();

  var diffPars = controls.checkIfChangeParams();
  if(diffPars.length)
  {
    updateParameters(diffPars);
    generateProcessData();
    initVizualization();
  }


  requestAnimationFrame(mainLoop);
}

requestAnimationFrame(mainLoop);
