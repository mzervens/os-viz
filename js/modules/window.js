/*jshint esversion: 6 */

class Window {
  constructor(notify) {
    this.updateSize();
    this.listening = false;
    this.notify = notify;
    this.listener();
  }

  updateSize() {
    this.width = $(window).width();
    this.height = $(window).height();
    console.log(this.width, this.height);
  }

  listener() {
    if(this.listening) {
      return;
    }

    var ref = this;
    ref.listening = true;

    $(window).on('resize', function(e)
    {
      ref.updateSize();
      ref.notify();
    });

  }

}



export {Window};
