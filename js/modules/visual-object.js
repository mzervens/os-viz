class VisualObject {
  constructor(graphics) {

    this.graphics = graphics;

  }

  genBoxIndices(ref, offset, offsetY = 0)
  {
    var out = [];
    var topLeftX = ref[0];
    var topLeftY = ref[1];
    var offsetX = offset;

    if(offsetY == 0)
    {
      offsetY = offsetX;
    }

    out.push([topLeftX, topLeftY]);
    out.push([topLeftX + offsetX, topLeftY]);
    out.push([topLeftX + offsetX, topLeftY + offsetY]);
    out.push([topLeftX, topLeftY + offsetY]);
    out.push([topLeftX, topLeftY]);

    return out;
  }

  render()
  {
    console.log("render not overridden")
  }
}

export {VisualObject};
