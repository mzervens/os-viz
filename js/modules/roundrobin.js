import {roundToDecPlaces} from './utility';

class RoundRobin {
  constructor(quantum, processContainer, infoField, avgIndxInInfo) {
    this.q = quantum;
    this.processQ = processContainer;
    this.info = infoField;
    this.avgInfoIndx = avgIndxInInfo;
    this.currProcess = null;
    this.qLeft = quantum;
    this.totalWaitSum = 0;
    this.waitTimeSumCount = 0;
  }


  update()
  {
    if(this.currProcess == null)
    {
      this.currProcess = this.processQ.getNextProcessIndx();
      if(this.currProcess == null)
        return;
    }
    if(this.qLeft <= 0)
    {

      var processWaited = this.processQ.at(this.currProcess).timeWaiting;
      this.waitTimeSumCount += 1;
      this.totalWaitSum += processWaited;

      if(!this.processQ.at(this.currProcess).done)
      {
        this.processQ.at(this.currProcess).timeWaiting = 0;
      }

      var burstsRecieved = this.q;
      var burstsHadWithThis = this.processQ.at(this.currProcess).burstsHad + this.q;

      if(burstsHadWithThis > this.processQ.at(this.currProcess).timeNeeded)
      {
        burstsRecieved = this.q - (burstsHadWithThis - this.processQ.at(this.currProcess).timeNeeded);
      }

      this.processQ.at(this.currProcess).burstsHad += this.q;
      this.processQ.updateTimeWaiting(burstsRecieved, this.currProcess);

      this.info.variableInfo[this.avgInfoIndx] = roundToDecPlaces((this.totalWaitSum / this.waitTimeSumCount), 2);

      this.currProcess = this.processQ.getNextProcessIndx(this.currProcess);
      this.qLeft = this.q;
      if(this.currProcess == null)
        return;
    }

    this.processQ.selectedProc = this.currProcess;
    var burstPerThick = 0.01 * this.q;

    this.processQ.at(this.currProcess).update(burstPerThick);
    this.qLeft -= burstPerThick;

  }
}

export {RoundRobin};
