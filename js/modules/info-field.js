import {VisualObject} from './visual-object';

class InfoField extends VisualObject {
  constructor(graphics, topLeft, heading, staticInfoLines, variableInfoLines, headingFont = "32px sans-serif", infoFont = "20px sans-serif") {
    super(graphics);
    this.topLeft = topLeft;
    this.heading = heading;
    this.information = staticInfoLines;
    this.variableInfo = variableInfoLines
    this.headingFont = headingFont;
    this.infoFont = infoFont;
  }

  buildInfoLines()
  {
    var res = [];
    for(var i = 0; i < this.information.length; i++)
    {
      res.push(this.information[i] + this.variableInfo[i].toString());
    }
    return res;
  }

  render()
  {
    this.graphics.renderText(this.heading, this.topLeft, 0, "#000000", this.headingFont, false);

    var infoPos = [this.topLeft[0] + 20, this.topLeft[1] + this.graphics.measureAproxTextHeight() + 10];
    this.graphics.renderTextLineByLine(this.buildInfoLines(), infoPos, 0, 8, "#000000", this.infoFont);
  }
}

export {InfoField};
