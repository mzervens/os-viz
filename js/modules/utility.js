
function randBetween(from, to)
{
  return Math.floor(Math.random() * (to - from + 1) + from);
}

function roundToDecPlaces(num, numberOfPlaces)
{
  var places = 10;
  for(var i = 1; i < numberOfPlaces; i++)
  {
    places * 10;
  }

  return Math.round(num * places) / places;
}

export {randBetween, roundToDecPlaces};
