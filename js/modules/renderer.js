/*jshint esversion: 6 */
import {Window} from './window';


class Renderer {
  constructor(elementId) {

    this.canvas = $(elementId)[0];
    this.window = new Window(() => {this.resizeCanvas()});
    this.resizeCanvas();
    this.defaultColor = "#000000";
  }

  getCtx()
  {
    return this.canvas.getContext("2d");
  }

  resizeCanvas()
  {
    $(this.canvas).attr('width', this.window.width);
    $(this.canvas).attr('height', this.window.height);
  }

  offsetByPercFromCanvSides(percX, percY)
  {
    var horOffs = this.canvas.width * percX;
    var verOffs = this.canvas.height * percY;

    var topLeft = [];
    var dims = [];

    topLeft[0] = horOffs;
    topLeft[1] = verOffs;

    dims[0] = this.canvas.width - (2 * horOffs);
    dims[1] = this.canvas.height - (2 * verOffs);

    return [topLeft, dims];
  }

  canvasCenter()
  {
    var center = [];
    center[0] = this.canvas.width / 2;
    center[1] = this.canvas.height / 2;

    return center;
  }

  clear()
  {
    var ctx = this.getCtx();
    ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  measureAproxTextHeight()
  {
    var ctx = this.getCtx();
    var aproxHeight = ctx.measureText('M').width;

    return aproxHeight;
  }

  renderImg(img, pos)
  {
    var ctx = this.getCtx();
    ctx.drawImage(img, pos[0], pos[1]);
  }

  renderText(text, coord, maxWidth = 0, color = "#000000", font = "18px sans-serif", doCentering = true)
  {
    var ctx = this.getCtx();
    ctx.fillStyle = color;
    ctx.font = font;

    if(doCentering)
    {
      var neededSize = ctx.measureText(text);
      neededSize.height = this.measureAproxTextHeight();

      coord[0] -= (neededSize.width / 2);
      coord[1] += (neededSize.height / 2);
    }

    if(maxWidth)
      ctx.fillText(text, coord[0], coord[1], maxWidth);
    else
      ctx.fillText(text, coord[0], coord[1]);
  }

  renderTextLineByLine(lines, coord, maxWidth = 0, lineAproxSep = 6, color = "#000000", font = "18px sans-serif")
  {
    coord[1] += lineAproxSep;

    for(var i = 0; i < lines.length; i++)
    {
      this.renderText(lines[i], coord, maxWidth, color, font, false);

      var lineHeight = this.measureAproxTextHeight();
      coord[1] += lineHeight + lineAproxSep;
    }
  }

  renderLine(pFrom, pTo, color = "#000000")
  {
    var ctx = this.getCtx();
    ctx.beginPath();
    ctx.moveTo(pFrom[0], pFrom[1]);
    ctx.lineTo(pTo[0], pTo[1]);

    ctx.strokeStyle = color;
    ctx.stroke();
  }

  // light blue color rgb(88, 175, 209)
  fillRectangle(top, sizes, color = "#000000")
  {
    var ctx = this.getCtx();
    ctx.fillStyle = color;
    ctx.fillRect(top[0], top[1], sizes[0], sizes[1]);

  }

  renderPath(pointList, color = "#000000")
  {
    if(pointList.length < 2)
    {
      return;
    }

    if(pointList.length == 2)
    {
      this.renderLine(pointList[0], pointList[1], color);
      return;
    }

    var ctx = this.getCtx();
    var fromPnt = pointList[0];
    var targetPnt;
    ctx.beginPath();

    for(var i = 1; i < pointList.length; i++)
    {
      targetPnt = pointList[i];

      ctx.moveTo(fromPnt[0], fromPnt[1]);
      ctx.lineTo(targetPnt[0], targetPnt[1]);
      fromPnt = targetPnt;

    }

    ctx.strokeStyle = color;
    ctx.stroke();
  }


}

export {Renderer};
