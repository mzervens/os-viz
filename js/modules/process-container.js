import {VisualObject} from './visual-object';
import {Process} from './process';

class ProcessContainer extends VisualObject {
  constructor(graphics, topLeft, maxWidth, maxVisibleProcesses) {
    super(graphics);
    this.processQueue = [];
    this.topLeft = topLeft;

    this.height = maxWidth / maxVisibleProcesses;
    this.width = maxWidth;
    this.maxVisProc = maxVisibleProcesses;

    this.pathPnts = [];
    this.selectedProc = null;

    this.calcPathPoints();

  }

  at(i)
  {
    return this.processQueue[i];
  }

  updateTimeWaiting(time, excluded)
  {
    for(var i = 0; i < this.processQueue.length; i++)
    {
      if(i == excluded)
      {
        continue;
      }
      if(i + 1 > this.maxVisProc)
      {
        break;
      }
      if(this.processQueue[i].done)
      {
        continue;
      }


      this.processQueue[i].timeWaiting += time;

    }
  }

  calcPathPoints()
  {
    this.pathPnts = this.genBoxIndices(this.topLeft, this.width, this.height);
  }

  addProcessInitPos(name, timeNeeded)
  {
    var xOffs = this.height * this.processQueue.length;
    var topLeft = [this.topLeft[0] + xOffs, this.topLeft[1]];
    var proc = new Process(this.graphics, name, topLeft, topLeft, this.height, timeNeeded);

    this.processQueue.push(proc);
  }

  addProcess(name, timeNeeded)
  {
    var xOffs = this.height * this.processQueue.length;
    var topLeft = [this.topLeft[0] + xOffs, this.topLeft[1]];
    var topLeftStart = [this.topLeft[0] + this.width, this.topLeft[1] - this.height];

    var proc = new Process(this.graphics, name, topLeft, topLeftStart, this.height, timeNeeded);

    this.processQueue.push(proc);
  }

  getNextProcessIndx(curr = -1)
  {
    for(var i = curr + 1; i < this.processQueue.length; i++)
    {
      if(i + 1 > this.maxVisProc)
      {
        break;
      }

      if(!this.processQueue[i].done)
      {
        return i;
      }
    }

    return null;
  }

  animate(multiplier = 1)
  {
    for(var i = 0; i < this.processQueue.length; i++)
    {
      if(i + 1 > this.maxVisProc)
      {
        break;
      }

      this.processQueue[i].animate(multiplier);
    }
  }

  render()
  {
    this.graphics.renderPath(this.pathPnts);
    var selectedProc = null;

    for(var i = 0; i < this.processQueue.length; i++)
    {
      if(i + 1 > this.maxVisProc)
      {
        break;
      }

      if(i == this.selectedProc)
      {
        selectedProc = this.processQueue[i];
        continue;
      }

      this.processQueue[i].render();
    }

    if(selectedProc != null)
    {
      selectedProc.render(true);
    }

  }

}

export {ProcessContainer};
