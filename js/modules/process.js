import {VisualObject} from './visual-object';
import {roundToDecPlaces} from './utility';

class Process extends VisualObject{
  constructor(graphics, name, topLeft, topLeftAnim, dim, timeNeeded, completionPerc = 0) {
    super(graphics);
    this.topLeft = topLeft;
    this.animationPos = topLeftAnim;
    this.size = dim;
    this.name = name;
    this.timeNeeded = timeNeeded;
    this.timeHad = 0;
    this.timeWaiting = 0;
    this.burstsHad = 0;
    this.compPerc = completionPerc;
    this.pathPnts = [];
    this.animation = false;
    this.done = false;
    this.highlightCol = "#FF0000";
    this.completionCol = "#58AFD1";

    if(topLeft[0] != topLeftAnim[0] || topLeft[1] !== topLeftAnim[1])
    {
      this.animation = true;
    }

  }

  update(burst)
  {
    this.timeHad += burst;
    if(this.timeHad < this.timeNeeded)
    {
      this.compPerc = roundToDecPlaces(this.timeHad / this.timeNeeded, 2);
    }
    else
    {
      this.compPerc = 1;
      this.done = true;
    }
  }

  buildHighlightArrowIndices(refPnt = [])
  {
    if(refPnt.length == 0)
    {
      refPnt = this.topLeft;
    }

    var centerX = refPnt[0] + (this.size / 2);
    var centerY = refPnt[1];
    var len = this.size / 4;
    var offs = this.size * 0.1;

    var topPoint = [centerX, centerY - len - offs];
    var botPoint = [centerX, centerY - offs];

    var arrowOffsY = len * 0.33 + offs;
    var arrowOffsX = arrowOffsY / 2;

    var leftPoint = [centerX - arrowOffsX, centerY - arrowOffsY];
    var rightPoint = [centerX + arrowOffsX, centerY - arrowOffsY];

    return [topPoint, botPoint, leftPoint, botPoint, rightPoint];
  }

  animate(multiplier = 1)
  {
    if(!this.animation)
    {
      return;
    }
    var updConst = 1;
    var updateSpeed = updConst * multiplier;

    if(this.animationPos[1] < this.topLeft[1])
    {
      this.animationPos[1] += updateSpeed;
      return;
    }
    this.animationPos[1] = this.topLeft[1];

    if(this.animationPos[0] > this.topLeft[0])
    {
      this.animationPos[0] -= updateSpeed;
      return;
    }
    this.animationPos[0] = this.topLeft[0];
    this.animation = false;
  }

  getLabelPos(refPnt = [])
  {
    var pos = this.topLeft;
    var out = [];
    if(refPnt.length)
    {
      pos = refPnt;
    }

    out[0] = pos[0] + (this.size / 2);
    out[1] = pos[1] + (this.size / 2);
    return out;
  }

  getTagetFillX(refPnt = [])
  {
    var fromX = this.topLeft[0];
    if(refPnt.length)
    {
      fromX = refPnt[0];
    }

    var offs = this.size * this.compPerc;
    return offs;
  }

  calcPathPoints(refPnt = [])
  {
    var pos = this.topLeft;
    if(refPnt.length)
    {
      pos = refPnt;
    }

    this.pathPnts = this.genBoxIndices(pos, this.size);
  }

  renderInfo(refPnt = [])
  {
    var startPos = this.topLeft;

    if(refPnt.length)
    {
      startPos = refPnt;
    }

    var offs = this.size * 0.1;
    var infoPos = [startPos[0] + offs, startPos[1] + this.size + offs];
    var burstTimeDelta = this.timeNeeded - this.burstsHad;
    if(burstTimeDelta < 0)
    {
      burstTimeDelta = 0;
    }

    this.graphics.renderTextLineByLine(["burst: " + this.timeNeeded,
                                        "burst left: " + burstTimeDelta,
                                        "waiting: " + this.timeWaiting.toString()
                                        ],
                                        infoPos,
                                        this.size - (offs * 2));
  }

  render(highlight = false)
  {
    var pos = this.topLeft;
    if(this.animation)
    {
      pos = this.animationPos;
    }
    this.calcPathPoints(pos);

    if(this.compPerc > 0 && this.compPerc < 100)
    {
      this.graphics.fillRectangle(pos, [this.getTagetFillX(pos), this.size], this.completionCol);
    }

    if(highlight)
    {
      this.graphics.renderPath(this.buildHighlightArrowIndices(pos), this.highlightCol);
      this.graphics.renderPath(this.pathPnts, this.highlightCol);
    }
    else
    {
      this.graphics.renderPath(this.pathPnts)
    }

    this.graphics.renderText(this.name, this.getLabelPos(pos), this.size);
    this.renderInfo(pos);
  }
}

export {Process};
