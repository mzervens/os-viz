import {VisualObject} from './visual-object';

class GraphicalInfoField extends VisualObject
{
  constructor(graphics, topLeft, imgList, textList)
  {
    super(graphics);
    this.topLeft = topLeft;
    this.textList = textList;
    this.images = [];
    this.assetsLoaded = false;

    for(var i = 0; i < imgList.length; i++)
    {
      var img = new Image();
      img.src = imgList[i];

      this.images.push(img);
    }

  }

  checkIfLoaded()
  {
    for(var i = 0; i < this.images.length; i++)
    {
      if(!this.images[i].complete || this.images[i].naturalWidth === 0)
      {
        return false;
      }
    }

    return true;
  }


  render()
  {
    if(!this.assetsLoaded)
    {
      this.assetsLoaded = this.checkIfLoaded();
      if(!this.assetsLoaded)
        return;
    }

    var pos = this.topLeft;
    var textPos;
    var offs = 10;

    for(var i = 0; i < this.images.length; i++)
    {
      this.graphics.renderImg(this.images[i], pos);
      textPos = [pos[0] + this.images[i].width + offs, pos[1] + this.images[i].height / 2];

      this.graphics.renderText(this.textList[i], textPos, 0, "#000000", "15px sans-serif", false);

      pos = [pos[0], pos[1] + this.images[i].height + offs];
    }

  }
}

export {GraphicalInfoField};
