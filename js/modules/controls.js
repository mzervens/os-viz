
class Controls {
  constructor() {
      this.paused = false;
      this.restart = false;
      this.menuEl = $("#menuModal");
      this.menuEl.modal({ show: false});
      this.changeParams = [];

      this.listen();

  }

  checkIfChangeParams()
  {
    if(this.changeParams.length)
    {
      var ret = this.changeParams;
      this.changeParams = [];
      return ret;
    }
    return [];
  }

  checkIfRestart()
  {
    if(this.restart)
    {
      this.restart = false;
      return true;
    }
    else
    {
      return false;
    }
  }

  pause()
  {
    this.paused = true;
  }

  inBounds(elements)
  {
    for(var i = 0; i < elements.length; i++)
    {
      var el = elements[i];
      var val = parseInt(el.val());
      if(el.is("[min]") && parseInt(el.attr("min")) > val)
      {
        console.log("failed on", el);
        return false;
      }
      if(el.is("[max]") && parseInt(el.attr("max")) < val)
      {
        console.log("failed ons", el);
        return false;
      }
    }
    return true;
  }

  listen()
  {
    var ref = this;

    $("#changeParams").click(function(e)
    {
      var quantum = parseInt($("#quantum").val());
      var processCount = parseInt($("#num-processes").val());

      var randFrom = parseInt($("#randFrom").val());
      var randTo = parseInt($("#randTo").val());

      if(ref.inBounds([$("#quantum"), $("#num-processes"), $("#randFrom"), $("#randTo")]))
      {
        console.log("valid");
        ref.changeParams = [quantum, processCount, randFrom, randTo];
        ref.menuEl.modal("hide");
      }
      console.log("invalid");


    });

    $(document).keypress(function(e)
    {
      if(e.which == 32)// space
      {
        if(ref.paused)
          ref.paused = false;
        else
          ref.paused = true;
      }
      else if(e.which == 77 || e.which == 109) // M
      {
        ref.menuEl.modal('show');
        ref.pause();
      }
      else if(e.which == 82 || e.which == 114) // R
      {
        ref.restart = true;
      }

    });
  }
}

export {Controls};
