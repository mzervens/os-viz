(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

exports.__esModule = true;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Controls = (function () {
  function Controls() {
    _classCallCheck(this, Controls);

    this.paused = false;
    this.restart = false;
    this.menuEl = $("#menuModal");
    this.menuEl.modal({ show: false });
    this.changeParams = [];

    this.listen();
  }

  Controls.prototype.checkIfChangeParams = function checkIfChangeParams() {
    if (this.changeParams.length) {
      var ret = this.changeParams;
      this.changeParams = [];
      return ret;
    }
    return [];
  };

  Controls.prototype.checkIfRestart = function checkIfRestart() {
    if (this.restart) {
      this.restart = false;
      return true;
    } else {
      return false;
    }
  };

  Controls.prototype.pause = function pause() {
    this.paused = true;
  };

  Controls.prototype.inBounds = function inBounds(elements) {
    for (var i = 0; i < elements.length; i++) {
      var el = elements[i];
      var val = parseInt(el.val());
      if (el.is("[min]") && parseInt(el.attr("min")) > val) {
        console.log("failed on", el);
        return false;
      }
      if (el.is("[max]") && parseInt(el.attr("max")) < val) {
        console.log("failed ons", el);
        return false;
      }
    }
    return true;
  };

  Controls.prototype.listen = function listen() {
    var ref = this;

    $("#changeParams").click(function (e) {
      var quantum = parseInt($("#quantum").val());
      var processCount = parseInt($("#num-processes").val());

      var randFrom = parseInt($("#randFrom").val());
      var randTo = parseInt($("#randTo").val());

      if (ref.inBounds([$("#quantum"), $("#num-processes"), $("#randFrom"), $("#randTo")])) {
        console.log("valid");
        ref.changeParams = [quantum, processCount, randFrom, randTo];
        ref.menuEl.modal("hide");
      }
      console.log("invalid");
    });

    $(document).keypress(function (e) {
      if (e.which == 32) // space
        {
          if (ref.paused) ref.paused = false;else ref.paused = true;
        } else if (e.which == 77 || e.which == 109) // M
        {
          ref.menuEl.modal('show');
          ref.pause();
        } else if (e.which == 82 || e.which == 114) // R
        {
          ref.restart = true;
        }
    });
  };

  return Controls;
})();

exports.Controls = Controls;

},{}],2:[function(require,module,exports){
"use strict";

exports.__esModule = true;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _visualObject = require('./visual-object');

var GraphicalInfoField = (function (_VisualObject) {
  _inherits(GraphicalInfoField, _VisualObject);

  function GraphicalInfoField(graphics, topLeft, imgList, textList) {
    _classCallCheck(this, GraphicalInfoField);

    _VisualObject.call(this, graphics);
    this.topLeft = topLeft;
    this.textList = textList;
    this.images = [];
    this.assetsLoaded = false;

    for (var i = 0; i < imgList.length; i++) {
      var img = new Image();
      img.src = imgList[i];

      this.images.push(img);
    }
  }

  GraphicalInfoField.prototype.checkIfLoaded = function checkIfLoaded() {
    for (var i = 0; i < this.images.length; i++) {
      if (!this.images[i].complete || this.images[i].naturalWidth === 0) {
        return false;
      }
    }

    return true;
  };

  GraphicalInfoField.prototype.render = function render() {
    if (!this.assetsLoaded) {
      this.assetsLoaded = this.checkIfLoaded();
      if (!this.assetsLoaded) return;
    }

    var pos = this.topLeft;
    var textPos;
    var offs = 10;

    for (var i = 0; i < this.images.length; i++) {
      this.graphics.renderImg(this.images[i], pos);
      textPos = [pos[0] + this.images[i].width + offs, pos[1] + this.images[i].height / 2];

      this.graphics.renderText(this.textList[i], textPos, 0, "#000000", "15px sans-serif", false);

      pos = [pos[0], pos[1] + this.images[i].height + offs];
    }
  };

  return GraphicalInfoField;
})(_visualObject.VisualObject);

exports.GraphicalInfoField = GraphicalInfoField;

},{"./visual-object":10}],3:[function(require,module,exports){
"use strict";

exports.__esModule = true;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _visualObject = require('./visual-object');

var InfoField = (function (_VisualObject) {
  _inherits(InfoField, _VisualObject);

  function InfoField(graphics, topLeft, heading, staticInfoLines, variableInfoLines) {
    var headingFont = arguments.length <= 5 || arguments[5] === undefined ? "32px sans-serif" : arguments[5];
    var infoFont = arguments.length <= 6 || arguments[6] === undefined ? "20px sans-serif" : arguments[6];

    _classCallCheck(this, InfoField);

    _VisualObject.call(this, graphics);
    this.topLeft = topLeft;
    this.heading = heading;
    this.information = staticInfoLines;
    this.variableInfo = variableInfoLines;
    this.headingFont = headingFont;
    this.infoFont = infoFont;
  }

  InfoField.prototype.buildInfoLines = function buildInfoLines() {
    var res = [];
    for (var i = 0; i < this.information.length; i++) {
      res.push(this.information[i] + this.variableInfo[i].toString());
    }
    return res;
  };

  InfoField.prototype.render = function render() {
    this.graphics.renderText(this.heading, this.topLeft, 0, "#000000", this.headingFont, false);

    var infoPos = [this.topLeft[0] + 20, this.topLeft[1] + this.graphics.measureAproxTextHeight() + 10];
    this.graphics.renderTextLineByLine(this.buildInfoLines(), infoPos, 0, 8, "#000000", this.infoFont);
  };

  return InfoField;
})(_visualObject.VisualObject);

exports.InfoField = InfoField;

},{"./visual-object":10}],4:[function(require,module,exports){
/*jshint esversion: 6 */
'use strict';

var _window = require('./window');

var _renderer = require('./renderer');

var _visualObject = require('./visual-object');

var _process = require('./process');

var _processContainer = require('./process-container');

var _infoField = require('./info-field');

var _utility = require('./utility');

var _controls = require('./controls');

var _roundrobin = require('./roundrobin');

var _graphicalInfoField = require('./graphical-info-field');

var quantum = 130;
var avgWait = 0;
var processCount = 4;
var randFrom = 1;
var randTo = 300;

var rend = new _renderer.Renderer("#canvas");
var positions = rend.offsetByPercFromCanvSides(0.1, 0.4);
var infoPos = rend.offsetByPercFromCanvSides(0.05, 0.1);
var visualInfoPos = rend.offsetByPercFromCanvSides(0.85, 0.1);

var visualInfo = new _graphicalInfoField.GraphicalInfoField(rend, visualInfoPos[0], ["assets/spacebar.png", "assets/key_m.png", "assets/key_r.png"], ["Stop / Play", "Change settings", "Restart"]);

var controls = new _controls.Controls();

var processCont = null;
var info = null;
var algo = null;
var processData = null;

function initVizualization() {
  processCont = new _processContainer.ProcessContainer(rend, positions[0], positions[1][0], processCount);
  info = new _infoField.InfoField(rend, infoPos[0], "Round robin algorithm", ["time quantum: ", "average wait time: "], [quantum, avgWait]);
  algo = new _roundrobin.RoundRobin(quantum, processCont, info, 1);

  for (var i = 0; i < processCount; i++) {
    processCont.addProcessInitPos(processData[i][0], processData[i][1]);
  }
}

function generateProcessData() {
  processData = [];

  for (var i = 0; i < processCount; i++) {
    var burst = _utility.randBetween(randFrom, randTo);
    var id = "P" + i.toString();
    processData.push([id, burst]);
  }
}

function updateParameters(parameters) {
  quantum = parameters[0];
  processCount = parameters[1];
  randFrom = parameters[2];
  randTo = parameters[3];
}

generateProcessData();
initVizualization();

function mainLoop() {
  rend.clear();
  info.render();
  visualInfo.render();
  processCont.render();
  processCont.animate(6);

  if (controls.paused == false) algo.update();

  if (controls.checkIfRestart()) initVizualization();

  var diffPars = controls.checkIfChangeParams();
  if (diffPars.length) {
    updateParameters(diffPars);
    generateProcessData();
    initVizualization();
  }

  requestAnimationFrame(mainLoop);
}

requestAnimationFrame(mainLoop);

},{"./controls":1,"./graphical-info-field":2,"./info-field":3,"./process":6,"./process-container":5,"./renderer":7,"./roundrobin":8,"./utility":9,"./visual-object":10,"./window":11}],5:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _visualObject = require('./visual-object');

var _process = require('./process');

var ProcessContainer = (function (_VisualObject) {
  _inherits(ProcessContainer, _VisualObject);

  function ProcessContainer(graphics, topLeft, maxWidth, maxVisibleProcesses) {
    _classCallCheck(this, ProcessContainer);

    _VisualObject.call(this, graphics);
    this.processQueue = [];
    this.topLeft = topLeft;

    this.height = maxWidth / maxVisibleProcesses;
    this.width = maxWidth;
    this.maxVisProc = maxVisibleProcesses;

    this.pathPnts = [];
    this.selectedProc = null;

    this.calcPathPoints();
  }

  ProcessContainer.prototype.at = function at(i) {
    return this.processQueue[i];
  };

  ProcessContainer.prototype.updateTimeWaiting = function updateTimeWaiting(time, excluded) {
    for (var i = 0; i < this.processQueue.length; i++) {
      if (i == excluded) {
        continue;
      }
      if (i + 1 > this.maxVisProc) {
        break;
      }
      if (this.processQueue[i].done) {
        continue;
      }

      this.processQueue[i].timeWaiting += time;
    }
  };

  ProcessContainer.prototype.calcPathPoints = function calcPathPoints() {
    this.pathPnts = this.genBoxIndices(this.topLeft, this.width, this.height);
  };

  ProcessContainer.prototype.addProcessInitPos = function addProcessInitPos(name, timeNeeded) {
    var xOffs = this.height * this.processQueue.length;
    var topLeft = [this.topLeft[0] + xOffs, this.topLeft[1]];
    var proc = new _process.Process(this.graphics, name, topLeft, topLeft, this.height, timeNeeded);

    this.processQueue.push(proc);
  };

  ProcessContainer.prototype.addProcess = function addProcess(name, timeNeeded) {
    var xOffs = this.height * this.processQueue.length;
    var topLeft = [this.topLeft[0] + xOffs, this.topLeft[1]];
    var topLeftStart = [this.topLeft[0] + this.width, this.topLeft[1] - this.height];

    var proc = new _process.Process(this.graphics, name, topLeft, topLeftStart, this.height, timeNeeded);

    this.processQueue.push(proc);
  };

  ProcessContainer.prototype.getNextProcessIndx = function getNextProcessIndx() {
    var curr = arguments.length <= 0 || arguments[0] === undefined ? -1 : arguments[0];

    for (var i = curr + 1; i < this.processQueue.length; i++) {
      if (i + 1 > this.maxVisProc) {
        break;
      }

      if (!this.processQueue[i].done) {
        return i;
      }
    }

    return null;
  };

  ProcessContainer.prototype.animate = function animate() {
    var multiplier = arguments.length <= 0 || arguments[0] === undefined ? 1 : arguments[0];

    for (var i = 0; i < this.processQueue.length; i++) {
      if (i + 1 > this.maxVisProc) {
        break;
      }

      this.processQueue[i].animate(multiplier);
    }
  };

  ProcessContainer.prototype.render = function render() {
    this.graphics.renderPath(this.pathPnts);
    var selectedProc = null;

    for (var i = 0; i < this.processQueue.length; i++) {
      if (i + 1 > this.maxVisProc) {
        break;
      }

      if (i == this.selectedProc) {
        selectedProc = this.processQueue[i];
        continue;
      }

      this.processQueue[i].render();
    }

    if (selectedProc != null) {
      selectedProc.render(true);
    }
  };

  return ProcessContainer;
})(_visualObject.VisualObject);

exports.ProcessContainer = ProcessContainer;

},{"./process":6,"./visual-object":10}],6:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _visualObject = require('./visual-object');

var _utility = require('./utility');

var Process = (function (_VisualObject) {
  _inherits(Process, _VisualObject);

  function Process(graphics, name, topLeft, topLeftAnim, dim, timeNeeded) {
    var completionPerc = arguments.length <= 6 || arguments[6] === undefined ? 0 : arguments[6];

    _classCallCheck(this, Process);

    _VisualObject.call(this, graphics);
    this.topLeft = topLeft;
    this.animationPos = topLeftAnim;
    this.size = dim;
    this.name = name;
    this.timeNeeded = timeNeeded;
    this.timeHad = 0;
    this.timeWaiting = 0;
    this.burstsHad = 0;
    this.compPerc = completionPerc;
    this.pathPnts = [];
    this.animation = false;
    this.done = false;
    this.highlightCol = "#FF0000";
    this.completionCol = "#58AFD1";

    if (topLeft[0] != topLeftAnim[0] || topLeft[1] !== topLeftAnim[1]) {
      this.animation = true;
    }
  }

  Process.prototype.update = function update(burst) {
    this.timeHad += burst;
    if (this.timeHad < this.timeNeeded) {
      this.compPerc = _utility.roundToDecPlaces(this.timeHad / this.timeNeeded, 2);
    } else {
      this.compPerc = 1;
      this.done = true;
    }
  };

  Process.prototype.buildHighlightArrowIndices = function buildHighlightArrowIndices() {
    var refPnt = arguments.length <= 0 || arguments[0] === undefined ? [] : arguments[0];

    if (refPnt.length == 0) {
      refPnt = this.topLeft;
    }

    var centerX = refPnt[0] + this.size / 2;
    var centerY = refPnt[1];
    var len = this.size / 4;
    var offs = this.size * 0.1;

    var topPoint = [centerX, centerY - len - offs];
    var botPoint = [centerX, centerY - offs];

    var arrowOffsY = len * 0.33 + offs;
    var arrowOffsX = arrowOffsY / 2;

    var leftPoint = [centerX - arrowOffsX, centerY - arrowOffsY];
    var rightPoint = [centerX + arrowOffsX, centerY - arrowOffsY];

    return [topPoint, botPoint, leftPoint, botPoint, rightPoint];
  };

  Process.prototype.animate = function animate() {
    var multiplier = arguments.length <= 0 || arguments[0] === undefined ? 1 : arguments[0];

    if (!this.animation) {
      return;
    }
    var updConst = 1;
    var updateSpeed = updConst * multiplier;

    if (this.animationPos[1] < this.topLeft[1]) {
      this.animationPos[1] += updateSpeed;
      return;
    }
    this.animationPos[1] = this.topLeft[1];

    if (this.animationPos[0] > this.topLeft[0]) {
      this.animationPos[0] -= updateSpeed;
      return;
    }
    this.animationPos[0] = this.topLeft[0];
    this.animation = false;
  };

  Process.prototype.getLabelPos = function getLabelPos() {
    var refPnt = arguments.length <= 0 || arguments[0] === undefined ? [] : arguments[0];

    var pos = this.topLeft;
    var out = [];
    if (refPnt.length) {
      pos = refPnt;
    }

    out[0] = pos[0] + this.size / 2;
    out[1] = pos[1] + this.size / 2;
    return out;
  };

  Process.prototype.getTagetFillX = function getTagetFillX() {
    var refPnt = arguments.length <= 0 || arguments[0] === undefined ? [] : arguments[0];

    var fromX = this.topLeft[0];
    if (refPnt.length) {
      fromX = refPnt[0];
    }

    var offs = this.size * this.compPerc;
    return offs;
  };

  Process.prototype.calcPathPoints = function calcPathPoints() {
    var refPnt = arguments.length <= 0 || arguments[0] === undefined ? [] : arguments[0];

    var pos = this.topLeft;
    if (refPnt.length) {
      pos = refPnt;
    }

    this.pathPnts = this.genBoxIndices(pos, this.size);
  };

  Process.prototype.renderInfo = function renderInfo() {
    var refPnt = arguments.length <= 0 || arguments[0] === undefined ? [] : arguments[0];

    var startPos = this.topLeft;

    if (refPnt.length) {
      startPos = refPnt;
    }

    var offs = this.size * 0.1;
    var infoPos = [startPos[0] + offs, startPos[1] + this.size + offs];
    var burstTimeDelta = this.timeNeeded - this.burstsHad;
    if (burstTimeDelta < 0) {
      burstTimeDelta = 0;
    }

    this.graphics.renderTextLineByLine(["burst: " + this.timeNeeded, "burst left: " + burstTimeDelta, "waiting: " + this.timeWaiting.toString()], infoPos, this.size - offs * 2);
  };

  Process.prototype.render = function render() {
    var highlight = arguments.length <= 0 || arguments[0] === undefined ? false : arguments[0];

    var pos = this.topLeft;
    if (this.animation) {
      pos = this.animationPos;
    }
    this.calcPathPoints(pos);

    if (this.compPerc > 0 && this.compPerc < 100) {
      this.graphics.fillRectangle(pos, [this.getTagetFillX(pos), this.size], this.completionCol);
    }

    if (highlight) {
      this.graphics.renderPath(this.buildHighlightArrowIndices(pos), this.highlightCol);
      this.graphics.renderPath(this.pathPnts, this.highlightCol);
    } else {
      this.graphics.renderPath(this.pathPnts);
    }

    this.graphics.renderText(this.name, this.getLabelPos(pos), this.size);
    this.renderInfo(pos);
  };

  return Process;
})(_visualObject.VisualObject);

exports.Process = Process;

},{"./utility":9,"./visual-object":10}],7:[function(require,module,exports){
/*jshint esversion: 6 */
"use strict";

exports.__esModule = true;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _window = require('./window');

var Renderer = (function () {
  function Renderer(elementId) {
    var _this = this;

    _classCallCheck(this, Renderer);

    this.canvas = $(elementId)[0];
    this.window = new _window.Window(function () {
      _this.resizeCanvas();
    });
    this.resizeCanvas();
    this.defaultColor = "#000000";
  }

  Renderer.prototype.getCtx = function getCtx() {
    return this.canvas.getContext("2d");
  };

  Renderer.prototype.resizeCanvas = function resizeCanvas() {
    $(this.canvas).attr('width', this.window.width);
    $(this.canvas).attr('height', this.window.height);
  };

  Renderer.prototype.offsetByPercFromCanvSides = function offsetByPercFromCanvSides(percX, percY) {
    var horOffs = this.canvas.width * percX;
    var verOffs = this.canvas.height * percY;

    var topLeft = [];
    var dims = [];

    topLeft[0] = horOffs;
    topLeft[1] = verOffs;

    dims[0] = this.canvas.width - 2 * horOffs;
    dims[1] = this.canvas.height - 2 * verOffs;

    return [topLeft, dims];
  };

  Renderer.prototype.canvasCenter = function canvasCenter() {
    var center = [];
    center[0] = this.canvas.width / 2;
    center[1] = this.canvas.height / 2;

    return center;
  };

  Renderer.prototype.clear = function clear() {
    var ctx = this.getCtx();
    ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  };

  Renderer.prototype.measureAproxTextHeight = function measureAproxTextHeight() {
    var ctx = this.getCtx();
    var aproxHeight = ctx.measureText('M').width;

    return aproxHeight;
  };

  Renderer.prototype.renderImg = function renderImg(img, pos) {
    var ctx = this.getCtx();
    ctx.drawImage(img, pos[0], pos[1]);
  };

  Renderer.prototype.renderText = function renderText(text, coord) {
    var maxWidth = arguments.length <= 2 || arguments[2] === undefined ? 0 : arguments[2];
    var color = arguments.length <= 3 || arguments[3] === undefined ? "#000000" : arguments[3];
    var font = arguments.length <= 4 || arguments[4] === undefined ? "18px sans-serif" : arguments[4];
    var doCentering = arguments.length <= 5 || arguments[5] === undefined ? true : arguments[5];

    var ctx = this.getCtx();
    ctx.fillStyle = color;
    ctx.font = font;

    if (doCentering) {
      var neededSize = ctx.measureText(text);
      neededSize.height = this.measureAproxTextHeight();

      coord[0] -= neededSize.width / 2;
      coord[1] += neededSize.height / 2;
    }

    if (maxWidth) ctx.fillText(text, coord[0], coord[1], maxWidth);else ctx.fillText(text, coord[0], coord[1]);
  };

  Renderer.prototype.renderTextLineByLine = function renderTextLineByLine(lines, coord) {
    var maxWidth = arguments.length <= 2 || arguments[2] === undefined ? 0 : arguments[2];
    var lineAproxSep = arguments.length <= 3 || arguments[3] === undefined ? 6 : arguments[3];
    var color = arguments.length <= 4 || arguments[4] === undefined ? "#000000" : arguments[4];
    var font = arguments.length <= 5 || arguments[5] === undefined ? "18px sans-serif" : arguments[5];

    coord[1] += lineAproxSep;

    for (var i = 0; i < lines.length; i++) {
      this.renderText(lines[i], coord, maxWidth, color, font, false);

      var lineHeight = this.measureAproxTextHeight();
      coord[1] += lineHeight + lineAproxSep;
    }
  };

  Renderer.prototype.renderLine = function renderLine(pFrom, pTo) {
    var color = arguments.length <= 2 || arguments[2] === undefined ? "#000000" : arguments[2];

    var ctx = this.getCtx();
    ctx.beginPath();
    ctx.moveTo(pFrom[0], pFrom[1]);
    ctx.lineTo(pTo[0], pTo[1]);

    ctx.strokeStyle = color;
    ctx.stroke();
  };

  // light blue color rgb(88, 175, 209)

  Renderer.prototype.fillRectangle = function fillRectangle(top, sizes) {
    var color = arguments.length <= 2 || arguments[2] === undefined ? "#000000" : arguments[2];

    var ctx = this.getCtx();
    ctx.fillStyle = color;
    ctx.fillRect(top[0], top[1], sizes[0], sizes[1]);
  };

  Renderer.prototype.renderPath = function renderPath(pointList) {
    var color = arguments.length <= 1 || arguments[1] === undefined ? "#000000" : arguments[1];

    if (pointList.length < 2) {
      return;
    }

    if (pointList.length == 2) {
      this.renderLine(pointList[0], pointList[1], color);
      return;
    }

    var ctx = this.getCtx();
    var fromPnt = pointList[0];
    var targetPnt;
    ctx.beginPath();

    for (var i = 1; i < pointList.length; i++) {
      targetPnt = pointList[i];

      ctx.moveTo(fromPnt[0], fromPnt[1]);
      ctx.lineTo(targetPnt[0], targetPnt[1]);
      fromPnt = targetPnt;
    }

    ctx.strokeStyle = color;
    ctx.stroke();
  };

  return Renderer;
})();

exports.Renderer = Renderer;

},{"./window":11}],8:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _utility = require('./utility');

var RoundRobin = (function () {
  function RoundRobin(quantum, processContainer, infoField, avgIndxInInfo) {
    _classCallCheck(this, RoundRobin);

    this.q = quantum;
    this.processQ = processContainer;
    this.info = infoField;
    this.avgInfoIndx = avgIndxInInfo;
    this.currProcess = null;
    this.qLeft = quantum;
    this.totalWaitSum = 0;
    this.waitTimeSumCount = 0;
  }

  RoundRobin.prototype.update = function update() {
    if (this.currProcess == null) {
      this.currProcess = this.processQ.getNextProcessIndx();
      if (this.currProcess == null) return;
    }
    if (this.qLeft <= 0) {

      var processWaited = this.processQ.at(this.currProcess).timeWaiting;
      this.waitTimeSumCount += 1;
      this.totalWaitSum += processWaited;

      if (!this.processQ.at(this.currProcess).done) {
        this.processQ.at(this.currProcess).timeWaiting = 0;
      }

      var burstsRecieved = this.q;
      var burstsHadWithThis = this.processQ.at(this.currProcess).burstsHad + this.q;

      if (burstsHadWithThis > this.processQ.at(this.currProcess).timeNeeded) {
        burstsRecieved = this.q - (burstsHadWithThis - this.processQ.at(this.currProcess).timeNeeded);
      }

      this.processQ.at(this.currProcess).burstsHad += this.q;
      this.processQ.updateTimeWaiting(burstsRecieved, this.currProcess);

      this.info.variableInfo[this.avgInfoIndx] = _utility.roundToDecPlaces(this.totalWaitSum / this.waitTimeSumCount, 2);

      this.currProcess = this.processQ.getNextProcessIndx(this.currProcess);
      this.qLeft = this.q;
      if (this.currProcess == null) return;
    }

    this.processQ.selectedProc = this.currProcess;
    var burstPerThick = 0.01 * this.q;

    this.processQ.at(this.currProcess).update(burstPerThick);
    this.qLeft -= burstPerThick;
  };

  return RoundRobin;
})();

exports.RoundRobin = RoundRobin;

},{"./utility":9}],9:[function(require,module,exports){
"use strict";

exports.__esModule = true;

function randBetween(from, to) {
  return Math.floor(Math.random() * (to - from + 1) + from);
}

function roundToDecPlaces(num, numberOfPlaces) {
  var places = 10;
  for (var i = 1; i < numberOfPlaces; i++) {
    places * 10;
  }

  return Math.round(num * places) / places;
}

exports.randBetween = randBetween;
exports.roundToDecPlaces = roundToDecPlaces;

},{}],10:[function(require,module,exports){
"use strict";

exports.__esModule = true;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var VisualObject = (function () {
  function VisualObject(graphics) {
    _classCallCheck(this, VisualObject);

    this.graphics = graphics;
  }

  VisualObject.prototype.genBoxIndices = function genBoxIndices(ref, offset) {
    var offsetY = arguments.length <= 2 || arguments[2] === undefined ? 0 : arguments[2];

    var out = [];
    var topLeftX = ref[0];
    var topLeftY = ref[1];
    var offsetX = offset;

    if (offsetY == 0) {
      offsetY = offsetX;
    }

    out.push([topLeftX, topLeftY]);
    out.push([topLeftX + offsetX, topLeftY]);
    out.push([topLeftX + offsetX, topLeftY + offsetY]);
    out.push([topLeftX, topLeftY + offsetY]);
    out.push([topLeftX, topLeftY]);

    return out;
  };

  VisualObject.prototype.render = function render() {
    console.log("render not overridden");
  };

  return VisualObject;
})();

exports.VisualObject = VisualObject;

},{}],11:[function(require,module,exports){
/*jshint esversion: 6 */

'use strict';

exports.__esModule = true;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var Window = (function () {
  function Window(notify) {
    _classCallCheck(this, Window);

    this.updateSize();
    this.listening = false;
    this.notify = notify;
    this.listener();
  }

  Window.prototype.updateSize = function updateSize() {
    this.width = $(window).width();
    this.height = $(window).height();
    console.log(this.width, this.height);
  };

  Window.prototype.listener = function listener() {
    if (this.listening) {
      return;
    }

    var ref = this;
    ref.listening = true;

    $(window).on('resize', function (e) {
      ref.updateSize();
      ref.notify();
    });
  };

  return Window;
})();

exports.Window = Window;

},{}]},{},[4]);
